#!/bin/sh
sed -i \
         -e 's/#000900/rgb(0%,0%,0%)/g' \
         -e 's/#00aa00/rgb(100%,100%,100%)/g' \
    -e 's/#000900/rgb(50%,0%,0%)/g' \
     -e 's/#00aa00/rgb(0%,50%,0%)/g' \
     -e 's/#000900/rgb(50%,0%,50%)/g' \
     -e 's/#00aa00/rgb(0%,0%,50%)/g' \
	"$@"
